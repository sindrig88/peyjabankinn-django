from django.core.management.base import BaseCommand

from peyjabanki.models import Game, BonusQuestion


class Command(BaseCommand):

    help = 'Calculates user scores'

    def calculate(self, game, guess):
        if self.winner(game) != self.winner(guess):
            return 0
        if game.home_score == guess.home_score and game.away_score == guess.away_score:
            return 5
        elif game.home_score == guess.home_score or game.away_score == guess.away_score:
            return 3
        return 1

    def winner(self, game):
        if game.home_score > game.away_score:
            return 1
        if game.home_score < game.away_score:
            return -1
        else:
            return 0

    def handle(self, *args, **options):

        games = Game.objects.exclude(
            calculated=True
        ).exclude(
            home_score__isnull=True
        ).exclude(
            away_score__isnull=True
        ).prefetch_related('userguess')

        for game in games:
            for userguess in game.userguess.all():
                userguess.points = self.calculate(game, userguess)
                userguess.save()
            game.calculated = True
            game.save()

        extras = BonusQuestion.objects.exclude(
            correct_answer__isnull=True
        ).prefetch_related('bonusguess')

        for extra in extras:
            for extraguess in extra.bonusguess.all():
                extraguess.points = 5 if extra.correct_answer == extraguess.answer else 0
                extraguess.save()
            extra.calculated = True
            extra.save()
