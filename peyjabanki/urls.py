from django.contrib.auth.decorators import login_required
from django.urls import path
from django.views.generic import TemplateView

from .views import (
    GuessView,
    ScoreView
)


urlpatterns = [
    path('', login_required(TemplateView.as_view(template_name='home.html')), name='home'),
    path('guess/', login_required(GuessView.as_view()), name='guess'),
    path('scores/', login_required(ScoreView.as_view()), name='scores'),
    path('rules/', login_required(TemplateView.as_view(template_name='rules.html')), name='rules'),
]
