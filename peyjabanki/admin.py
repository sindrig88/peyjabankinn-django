from django.contrib import admin

from peyjabanki.models import (
    BonusChoices,
    BonusQuestion,
    BonusQuestionChoices,
    Competition,
    Game,
    Team,
)

admin.site.register(BonusChoices)
admin.site.register(Competition)
admin.site.register(Game)
admin.site.register(Team)


class BonusQuestionChoicesInline(admin.TabularInline):
    model = BonusQuestionChoices
    extra = 1


class BonusQuestionAdmin(admin.ModelAdmin):
    inlines = (BonusQuestionChoicesInline, )


admin.site.register(BonusQuestion, BonusQuestionAdmin)
