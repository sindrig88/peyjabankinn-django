from django.db import models

from django.contrib.auth.models import User


class Competition(models.Model):
    short_name = models.CharField(max_length=20)
    name = models.CharField(max_length=200)
    start_date = models.DateField()

    def __str__(self):
        return self.short_name


class Team(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Game(models.Model):
    GROUP_CHOICES = (
        ('G', 'Group stages'),
        ('M', 'Main round')
    )

    home_team = models.ForeignKey(Team, null=True, on_delete=models.SET_NULL, related_name='hometeam')
    away_team = models.ForeignKey(Team, null=True, on_delete=models.SET_NULL, related_name='awayteam')
    gametime = models.DateTimeField()
    home_score = models.IntegerField(blank=True, null=True)
    away_score = models.IntegerField(blank=True, null=True)
    calculated = models.BooleanField()
    competition = models.ForeignKey(Competition, on_delete=models.SET_NULL, null=True, blank=True)
    group = models.CharField(max_length=1, choices=GROUP_CHOICES, default='G')

    def __str__(self):
        return "{} - {}".format(self.home_team, self.away_team)


class UserGuess(models.Model):
    game = models.ForeignKey(Game, on_delete=models.CASCADE, related_name='userguess')
    home_score = models.IntegerField(blank=True, null=True)
    away_score = models.IntegerField(blank=True, null=True)
    points = models.IntegerField(blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='userguess')


class BonusChoices(models.Model):
    choice = models.CharField(max_length=200)

    def __str__(self):
        return self.choice


class BonusQuestion(models.Model):
    question = models.CharField(max_length=500)
    correct_answer = models.ForeignKey(
        BonusChoices, null=True, blank=True, on_delete=models.SET_NULL, related_name='bonusanswer'
    )
    choices = models.ManyToManyField(BonusChoices, through='BonusQuestionChoices', related_name='bonuschoices')
    competition = models.ForeignKey(Competition, on_delete=models.SET_NULL, null=True, blank=True)
    calculated = models.BooleanField()

    def __str__(self):
        return self.question


class BonusQuestionChoices(models.Model):
    choice = models.ForeignKey(BonusChoices, on_delete=models.CASCADE)
    question = models.ForeignKey(BonusQuestion, on_delete=models.CASCADE, related_name='bonuschoices')


class UserBonusAnswer(models.Model):
    question = models.ForeignKey(BonusQuestion, on_delete=models.CASCADE, related_name='bonusguess')
    answer = models.ForeignKey(BonusQuestionChoices, on_delete=models.CASCADE, related_name='bonusguess', null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='bonusguess')
    points = models.IntegerField(blank=True, null=True)
